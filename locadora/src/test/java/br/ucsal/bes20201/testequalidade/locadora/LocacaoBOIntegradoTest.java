package br.ucsal.bes20201.testequalidade.locadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBOIntegradoTest {

	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {

		Veiculo veiculo1 = VeiculoBuilder.VeiculoAno2012().Build();
		Veiculo veiculo2 = VeiculoBuilder.VeiculoAno2012().Build();
		Veiculo veiculo3 = VeiculoBuilder.VeiculoAno2012().Build();
		Veiculo veiculo4 = VeiculoBuilder.VeiculoAno2019().Build();
		Veiculo veiculo5 = VeiculoBuilder.VeiculoAno2019().Build();

		VeiculoDAO.insert(veiculo1);
		VeiculoDAO.insert(veiculo2);
		VeiculoDAO.insert(veiculo3);
		VeiculoDAO.insert(veiculo4);
		VeiculoDAO.insert(veiculo5);

		List<Veiculo> veiculos = new ArrayList<Veiculo>();

		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);

		assertEquals(4935.0, LocacaoBO.calcularValorTotalLocacao(veiculos, 3));
	}

}
