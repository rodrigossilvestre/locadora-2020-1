package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.*;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private final static String PLACA_DEFAULT = "LOV6548";

	private final static Integer ANO_DEFAULT = 2020;

	private final static Modelo MODELO_DEFAULT = new Modelo("Esportivo");

	private final static Double ValorDIARIA_DEFAULT = 350d;

	private final static SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa;

	private Integer ano;

	private Modelo modelo;

	private Double valorDiaria;

	private SituacaoVeiculoEnum situacao;

	public static VeiculoBuilder VeiculoDefault() {
		return VeiculoBuilder.BuildarVeiculo().comPlaca(PLACA_DEFAULT).comAno(ANO_DEFAULT).comModelo(MODELO_DEFAULT)
				.comValorDiaria(ValorDIARIA_DEFAULT).comSituacao(SITUACAO_DEFAULT);
	}

	public VeiculoBuilder() {

	}

	public static VeiculoBuilder BuildarVeiculo() {
		return new VeiculoBuilder();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder comSituacaoDisponivel(SituacaoVeiculoEnum situacao) {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder comSituacaoManutencao(SituacaoVeiculoEnum situacao) {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder comSituacaoLocado(SituacaoVeiculoEnum situacao) {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public static VeiculoBuilder VeiculoAno2012() {
		return VeiculoBuilder.BuildarVeiculo().comAno(2012);
	}

	public static VeiculoBuilder VeiculoAno2019() {
		return VeiculoBuilder.BuildarVeiculo().comAno(2019);
	}

	public VeiculoBuilder but() {
		return VeiculoBuilder.BuildarVeiculo().comPlaca(this.placa).comAno(this.ano).comModelo(this.modelo)
				.comValorDiaria(this.valorDiaria).comSituacao(this.situacao);
	}

	public VeiculoBuilder(String placa, Integer ano, Modelo modelo, Double valorDiaria, SituacaoVeiculoEnum situacao) {
		super();
		this.placa = placa;
		this.ano = ano;
		this.modelo = modelo;
		this.valorDiaria = valorDiaria;
		this.situacao = situacao;
	}

	public Veiculo Build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(this.placa);
		veiculo.setAno(this.ano);
		veiculo.setModelo(this.modelo);
		veiculo.setValorDiaria(this.valorDiaria);
		veiculo.setSituacao(this.situacao);
		return veiculo;
	}
}
